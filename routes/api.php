<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\PassportAuthController;
use App\Http\Controllers\API\ArticleController;
use App\Http\Controllers\API\ArticleCategoryController;
use App\Http\Controllers\API\UserController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('register', [PassportAuthController::class, 'register']);
Route::post('login', [PassportAuthController::class, 'login']);


Route::middleware(['auth:api', 'role:admin,author'])->group(function () {
    Route::resource('article', ArticleController::class);
    Route::get('article/search/{keyword}', [ArticleController::class, 'search']);
    Route::get('article/search/detail/{keyword}', [ArticleController::class, 'searchDetailArtikel']);
    Route::get('article-category/search/{keyword}', [ArticleCategoryController::class, 'search']);
    Route::resource('article-category', ArticleCategoryController::class);
});

Route::middleware(['auth:api', 'role:admin'])->group(function () {
    Route::get('get-user', [PassportAuthController::class, 'userInfo']);
    Route::resource('user', UserController::class);
});
