<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Article;
use App\Models\ArticleCategory;
use Inertia\Inertia;
use Validator;
use Str;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $articles = Article::all(); //MEMBUAT QUERY UNTUK MENGAMBIL DATA DARI TABLE articles
        return Inertia::render('Article', ['data' => $articles]); //Return data with inertia
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            // 'slug' => 'required',
            'category_id' => 'required',
            'content' => 'required',
            'banner' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:2048'
        ]);

        // //check if validation fails
        // if ($validator->fails()) {
        //     // return response()->json($validator->errors(), 422);
        //     return $this->sendError('Validation Error.', $validator->errors());
        // }

        // $file = $request->file('banner')->store('banner', 'public');

        if ($request->file('banner') == null) {
            $file = "";
        }else{
           $file = $request->file('banner')->store('banner', 'public');
        }

        $article = Article::create([
            'title' => $request->title,
            'category_id' => $request->category_id,
            'slug' => Str::slug($request->title, '-'),
            'content' => $request->content,
            'banner' => $file,
        ]);

        // Validator::make($request->all(), [
        //     'code' => ['required', 'string', 'unique:products,code'],
        //     'name' => ['required', 'string'],
        //     'price' => ['required', 'integer'],
        //     'weight' => ['required', 'integer'],
        // ])->validate();

        // Product::create($request->all()); //MEMBUAT QUERY UNTUK MENYIMPAN DATA
        return redirect()->back()->with(['message' => 'Article: ' . $request->title . ' Ditambahkan']); //MENGIRIMK
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Article $article)
    {
        $validator = Validator::make($request->all(), [
            'title'         => 'required',
            'category_id'   => 'required',
            'content'       => 'required',
        ]);

        //check if validation fails
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        //check if banner is not empty
        if ($request->hasFile('banner')) {

            //upload banner
            $banner = $request->file('banner');
            $banner->store('banner', 'public');

            //delete old banner
            Storage::delete('banner/'.$article->banner);

            //update Article with new banner
            $article->update([
                'banner'     => $banner->hashName(),
                'title'     => $request->title,
                'slug' => Str::slug($request->title, '-'),
                'content'   => $request->content,
                'category_id'   => $request->category_id,
            ]);

        } else {
            //update Article without banner
            $article->update([
                'title'     => $request->title,
                'slug' => Str::slug($request->title, '-'),
                'content'   => $request->content,
                'category_id'   => $request->category_id,
            ]);
        }
        return redirect()->back()->with(['message' => 'artikel: ' . $request->title . ' Diperbaharui']); //KIRIM FLASH MESSAGE
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $article = Article::find($id);
        $article->delete();
        return redirect()->back()->with(['message' => 'article: ' . $article->title . ' Di hapus']);
    }
}
