<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Models\Article;
use Validator;
use Str;
use App\Http\Resources\ArticleResource as ArticleResource;

class ArticleController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $article = Article::with(['articleCategory'])->orderBy('created_at', 'desc')->paginate(3);

        return response()->json([
            "status" => true,
            "message" => "All Article retrieved",
            "data" => $article
        ]);
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            // 'slug' => 'required',
            'category_id' => 'required',
            'content' => 'required',
            'banner' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:2048'
        ]);

        //check if validation fails
        if ($validator->fails()) {
            // return response()->json($validator->errors(), 422);
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $banner_path = $request->file('banner')->store('banner', 'public');
        $article = Article::create([
            'title' => $request->title,
            'category_id' => $request->category_id,
            'slug' => Str::slug($request->title, '-'),
            'content' => $request->content,
            'banner' => $banner_path,
        ]);

        // return new ArticleResource(true, 'Data Artikel Berhasil Ditambahkan!', $article);
        return $this->sendResponse(new ArticleResource($article), 'article Created Successfully.');
    }

    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */

    public function show($id)
    {
        $article = Article::with(['articleCategory'])->find($id);
        if (!$article) {
            return response()->json([
                "message" => "Article Not Found",
            ], 404);
        }

        return $this->sendResponse(new ArticleResource($article), 'Article Retrieved Successfully.');
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */

    public function update(Request $request, Article $article)
    {
        $validator = Validator::make($request->all(), [
            'title'         => 'required',
            'category_id'   => 'required',
            'content'       => 'required',
        ]);

        //check if validation fails
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        //check if banner is not empty
        if ($request->hasFile('banner')) {

            //upload banner
            $banner = $request->file('banner');
            $banner->store('banner', 'public');

            //delete old banner
            Storage::delete('banner/'.$article->banner);

            //update Article with new banner
            $article->update([
                'banner'     => $banner->hashName(),
                'title'     => $request->title,
                'slug' => Str::slug($request->title, '-'),
                'content'   => $request->content,
                'category_id'   => $request->category_id,
            ]);

        } else {
            //update Article without banner
            $article->update([
                'title'     => $request->title,
                'slug' => Str::slug($request->title, '-'),
                'content'   => $request->content,
                'category_id'   => $request->category_id,
            ]);
        }

        return $this->sendResponse(new ArticleResource($article), 'Article Updated Successfully.');
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy(Article $article)
    {
        $article->delete();

        if(\Storage::exists('banner/'. $article->banner)){
            \Storage::delete('banner/'. $article->banner);
        }else{
        return response()->json([
            "message" => "Banner not found"
        ]);
        }
        return $this->sendResponse([], 'Product Deleted Successfully.');
    }

    public function search($keyword)
    {
        return Article::with(['articleCategory'])->where('title', "like", "%".$keyword."%")->orderBy('created_at', 'desc')->paginate(10);
    }

    public function searchDetailArtikel($keyword)
    {
        return Article::with(['articleCategory'])->where('slug', "like", "%".$keyword."%")->orderBy('created_at', 'desc')->paginate(10);
    }
}
