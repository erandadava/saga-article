<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ArticleCategory;
use Validator;
use Str;
use App\Http\Resources\ArticleCategoryResource as ArticleCategoryResource;

class ArticleCategoryController extends BaseController
{
    public function index()
    {
        $articleCategory = ArticleCategory::orderBy('created_at', 'desc')->paginate(3);

        return response()->json([
            "status" => true,
            "message" => "All ArticleCategory retrieved",
            "data" => $articleCategory
        ]);
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
        ]);

        //check if validation fails
        if ($validator->fails()) {
            // return response()->json($validator->errors(), 422);
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $articleCategory = ArticleCategory::create([
            'name' => $request->name,
            'slug' => Str::slug($request->name, '-'),
        ]);

        // return new ArticleCategoryResource(true, 'Data Artikel Berhasil Ditambahkan!', $articleCategory);
        return $this->sendResponse(new ArticleCategoryResource($articleCategory), 'Article Category Created Successfully.');
    }

    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */

    public function show($id)
    {
        $articleCategory = ArticleCategory::find($id);
        if (!$articleCategory) {
            return response()->json([
                "message" => "Article Not Found",
            ], 404);
        }

        return $this->sendResponse(new ArticleCategoryResource($articleCategory), 'Article Category Retrieved Successfully.');
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */

    public function update(Request $request, ArticleCategory $articleCategory)
    {
        $validator = Validator::make($request->all(), [
            'name'  => 'required',
        ]);

        //check if validation fails
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        //update Article without banner
        $articleCategory->update([
            'name'  => $request->name,
            'slug'  => Str::slug($request->name, '-'),
        ]);

        return $this->sendResponse(new ArticleCategoryResource($articleCategory), 'Article Updated Successfully.');
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy(ArticleCategory $articleCategory)
    {
        $articleCategory->delete();
        return $this->sendResponse([], 'Product Deleted Successfully.');
    }

    public function search($keyword)
    {
        return ArticleCategory::where('slug', "like", "%".$keyword."%")->orderBy('created_at', 'ASC')->paginate(10);
    }
}
