<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Validator;
use Str;
use App\Http\Resources\UserResource as UserResource;


class UserController extends BaseController
{
    public function index()
    {
        $user = User::orderBy('created_at', 'desc')->paginate(10);

        return response()->json([
            "status" => true,
            "message" => "All User retrieved",
            "data" => $user
        ]);
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|min:4',
            'email' => 'required|email',
            'password' => 'required|min:8',
            'role' => 'in:author,admin',
        ]);

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'role' => $request->role,
            'password' => bcrypt($request->password),
        ]);

        // $token = $user->createToken('Laravel9PassportAuth')->accessToken;

        // return response()->json([
        //     'message' => "Success",
        //     'token' => $token
        // ], 200);

        // $validator = Validator::make($request->all(), [
        //     'name' => 'required',
        // ]);

        // //check if validation fails
        // if ($validator->fails()) {
        //     // return response()->json($validator->errors(), 422);
        //     return $this->sendError('Validation Error.', $validator->errors());
        // }

        // $user = User::create([
        //     'name' => $request->name,
        //     'slug' => Str::slug($request->name, '-'),
        // ]);

        // return new UserResource(true, 'Data Artikel Berhasil Ditambahkan!', $user);
        dd($user);
        return $this->sendResponse(new UserResource($user), 'Article Category Created Successfully.');
    }

    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */

    public function show($id)
    {
        $user = User::find($id);
        if (!$user) {
            return response()->json([
                "message" => "User Not Found",
            ], 404);
        }
        return $this->sendResponse(new UserResource($user), 'User Retrieved Successfully.');
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */

    public function update(Request $request, User $user)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|min:4',
            'email' => 'required|email',
            'password' => 'required|min:8',
            'role' => 'in:author,admin',
        ]);

        //check if validation fails
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        //update Article without banner
        $user->update([
            'name' => $request->name,
            'email' => $request->email,
            'role' => $request->role,
            'password' => bcrypt($request->password),
        ]);

        return $this->sendResponse(new UserResource($user), 'User Updated Successfully.');
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy(User $user)
    {
        $user->delete();
        return $this->sendResponse([], 'Product Deleted Successfully.');
    }
}
